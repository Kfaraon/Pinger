﻿namespace Pinger
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label5555 = new System.Windows.Forms.Label();
            this.label4444 = new System.Windows.Forms.Label();
            this.label3333 = new System.Windows.Forms.Label();
            this.label2222 = new System.Windows.Forms.Label();
            this.label1111 = new System.Windows.Forms.Label();
            this.label55555 = new System.Windows.Forms.Label();
            this.label44444 = new System.Windows.Forms.Label();
            this.label33333 = new System.Windows.Forms.Label();
            this.label22222 = new System.Windows.Forms.Label();
            this.label11111 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(145, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(291, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "...";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(13, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 15);
            this.label11.TabIndex = 5;
            this.label11.Text = "...";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(145, 45);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 15);
            this.label22.TabIndex = 6;
            this.label22.Text = "...";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(291, 45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(16, 15);
            this.label33.TabIndex = 7;
            this.label33.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(433, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(569, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "...";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(433, 45);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(16, 15);
            this.label44.TabIndex = 11;
            this.label44.Text = "...";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(569, 45);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(16, 15);
            this.label55.TabIndex = 12;
            this.label55.Text = "...";
            // 
            // label5555
            // 
            this.label5555.AutoSize = true;
            this.label5555.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5555.ForeColor = System.Drawing.Color.Black;
            this.label5555.Location = new System.Drawing.Point(569, 75);
            this.label5555.Name = "label5555";
            this.label5555.Size = new System.Drawing.Size(16, 15);
            this.label5555.TabIndex = 22;
            this.label5555.Text = "...";
            // 
            // label4444
            // 
            this.label4444.AutoSize = true;
            this.label4444.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4444.ForeColor = System.Drawing.Color.Black;
            this.label4444.Location = new System.Drawing.Point(433, 75);
            this.label4444.Name = "label4444";
            this.label4444.Size = new System.Drawing.Size(16, 15);
            this.label4444.TabIndex = 21;
            this.label4444.Text = "...";
            // 
            // label3333
            // 
            this.label3333.AutoSize = true;
            this.label3333.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3333.ForeColor = System.Drawing.Color.Black;
            this.label3333.Location = new System.Drawing.Point(291, 75);
            this.label3333.Name = "label3333";
            this.label3333.Size = new System.Drawing.Size(16, 15);
            this.label3333.TabIndex = 20;
            this.label3333.Text = "...";
            // 
            // label2222
            // 
            this.label2222.AutoSize = true;
            this.label2222.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2222.ForeColor = System.Drawing.Color.Black;
            this.label2222.Location = new System.Drawing.Point(145, 75);
            this.label2222.Name = "label2222";
            this.label2222.Size = new System.Drawing.Size(16, 15);
            this.label2222.TabIndex = 19;
            this.label2222.Text = "...";
            // 
            // label1111
            // 
            this.label1111.AutoSize = true;
            this.label1111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1111.ForeColor = System.Drawing.Color.Black;
            this.label1111.Location = new System.Drawing.Point(13, 75);
            this.label1111.Name = "label1111";
            this.label1111.Size = new System.Drawing.Size(16, 15);
            this.label1111.TabIndex = 18;
            this.label1111.Text = "...";
            // 
            // label55555
            // 
            this.label55555.AutoSize = true;
            this.label55555.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55555.ForeColor = System.Drawing.Color.Black;
            this.label55555.Location = new System.Drawing.Point(569, 10);
            this.label55555.Name = "label55555";
            this.label55555.Size = new System.Drawing.Size(16, 15);
            this.label55555.TabIndex = 28;
            this.label55555.Text = "...";
            // 
            // label44444
            // 
            this.label44444.AutoSize = true;
            this.label44444.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44444.ForeColor = System.Drawing.Color.Black;
            this.label44444.Location = new System.Drawing.Point(433, 10);
            this.label44444.Name = "label44444";
            this.label44444.Size = new System.Drawing.Size(16, 15);
            this.label44444.TabIndex = 27;
            this.label44444.Text = "...";
            // 
            // label33333
            // 
            this.label33333.AutoSize = true;
            this.label33333.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33333.ForeColor = System.Drawing.Color.Black;
            this.label33333.Location = new System.Drawing.Point(291, 10);
            this.label33333.Name = "label33333";
            this.label33333.Size = new System.Drawing.Size(16, 15);
            this.label33333.TabIndex = 26;
            this.label33333.Text = "...";
            // 
            // label22222
            // 
            this.label22222.AutoSize = true;
            this.label22222.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22222.ForeColor = System.Drawing.Color.Black;
            this.label22222.Location = new System.Drawing.Point(145, 10);
            this.label22222.Name = "label22222";
            this.label22222.Size = new System.Drawing.Size(16, 15);
            this.label22222.TabIndex = 25;
            this.label22222.Text = "...";
            // 
            // label11111
            // 
            this.label11111.AutoSize = true;
            this.label11111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11111.ForeColor = System.Drawing.Color.Black;
            this.label11111.Location = new System.Drawing.Point(13, 10);
            this.label11111.Name = "label11111";
            this.label11111.Size = new System.Drawing.Size(16, 15);
            this.label11111.TabIndex = 24;
            this.label11111.Text = "...";
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.White;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 173);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(684, 208);
            this.listBox1.TabIndex = 30;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(684, 381);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label55555);
            this.Controls.Add(this.label44444);
            this.Controls.Add(this.label33333);
            this.Controls.Add(this.label22222);
            this.Controls.Add(this.label11111);
            this.Controls.Add(this.label5555);
            this.Controls.Add(this.label4444);
            this.Controls.Add(this.label3333);
            this.Controls.Add(this.label2222);
            this.Controls.Add(this.label1111);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Pinger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label5555;
        private System.Windows.Forms.Label label4444;
        private System.Windows.Forms.Label label3333;
        private System.Windows.Forms.Label label2222;
        private System.Windows.Forms.Label label1111;
        private System.Windows.Forms.Label label55555;
        private System.Windows.Forms.Label label44444;
        private System.Windows.Forms.Label label33333;
        private System.Windows.Forms.Label label22222;
        private System.Windows.Forms.Label label11111;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}

