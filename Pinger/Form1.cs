﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Reflection.Emit;
using System.IO;

namespace Pinger
{
    public partial class Form1 : Form
    {
        private PingCl _PingCl;
        public SynchronizationContext _context;
        public Thread thread1;
        public INIManager iniFile;
        public StreamWriter LogFile;

        string[] Host = new string[5];
        bool[] OnOff = new bool[3] { true, true, true };
        bool OnFlag  = false;
        bool AppStart = false;
        string h1_name,h1_adr,h2_name,h2_adr,h3_name,h3_adr,h4_name,h4_adr,h5_name,h5_adr;

        public Form1()
        {
            InitializeComponent();
            Load += Form1_Load;

            try
            {
                iniFile = new INIManager(Application.StartupPath + "\\settings.ini");
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ошибка: settings.ini" + "Exception: " + ee.Message);
                Application.Exit();
            }
            finally
            {
                //Получить значение по ключу name из секции main
                h1_name = iniFile.GetPrivateString("HOST", "h1_name");
                h1_adr  = iniFile.GetPrivateString("HOST", "h1_adr");
                h2_name = iniFile.GetPrivateString("HOST", "h2_name");
                h2_adr  = iniFile.GetPrivateString("HOST", "h2_adr");
                h3_name = iniFile.GetPrivateString("HOST", "h3_name");
                h3_adr  = iniFile.GetPrivateString("HOST", "h3_adr");
                h4_name = iniFile.GetPrivateString("HOST", "h4_name");
                h4_adr  = iniFile.GetPrivateString("HOST", "h4_adr");
                h5_name = iniFile.GetPrivateString("HOST", "h5_name");
                h5_adr  = iniFile.GetPrivateString("HOST", "h5_adr");

                label11111.Text = h1_name;
                label22222.Text = h2_name;
                label33333.Text = h3_name;
                label44444.Text = h4_name;
                label55555.Text = h5_name;

                Host[0] = h1_adr;
                Host[1] = h2_adr;
                Host[2] = h3_adr;
                Host[3] = h4_adr;
                Host[4] = h5_adr;
            }

            try
            {
                LogFile = File.AppendText(Application.StartupPath + "\\log.txt");
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ошибка: log.txt" + "Exception: " + ee.Message);
                Application.Exit();
            }
            finally
            {
                listBox1.Items.Add(DateTime.Now.ToString() + " - Запуск программы");
                LogFile.WriteLine(DateTime.Now.ToString() + " - Запуск программы");
                timer2.Enabled = true;

                AppStart = true;
            }
        }

        private void ThreadPingCl()
        {
            _PingCl = new PingCl();
            _PingCl.HostPingReply += HostPingReplyL1;
            _PingCl.HostPingReply2 += HostPingReplyL2;
            _PingCl.HostPingReply3 += HostPingReplyL3;
            _PingCl.HostPingReply4 += HostPingReplyL4;
            _PingCl.HostPingReply5 += HostPingReplyL5;

            thread1 = new Thread(() => _PingCl.SendPing(_context, Host));
            thread1.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _context = SynchronizationContext.Current;
        }

        private void HostPingReplyL1(System.Net.NetworkInformation.PingReply HostPingReply)
        {
            if (HostPingReply.Status == System.Net.NetworkInformation.IPStatus.Success) {
                label1.Text = HostPingReply.RoundtripTime.ToString() + " ms";
                label11.Text = "ip: " + HostPingReply.Address.ToString();
                label1111.Text = "TTL: " + HostPingReply.Options.Ttl.ToString();

                OnOff[0] = true;
            }
            else
            {
                label1.Text = "-";
                label11.Text = "-";
                label1111.Text = "-";

                OnOff[0] = false;
            }
        }

        private void HostPingReplyL2(System.Net.NetworkInformation.PingReply HostPingReply2)
        {
            if (HostPingReply2.Status == System.Net.NetworkInformation.IPStatus.Success)
            {
                label2.Text = HostPingReply2.RoundtripTime.ToString() + " ms";
                label22.Text = "ip: " + HostPingReply2.Address.ToString();
                label2222.Text = "TTL: " + HostPingReply2.Options.Ttl.ToString();

                OnOff[1] = true;
            }
            else
            {
                label2.Text = "-";
                label22.Text = "-";
                label2222.Text = "-";

                OnOff[1] = false;
            }
        }

        private void HostPingReplyL3(System.Net.NetworkInformation.PingReply HostPingReply3)
        {
            if (HostPingReply3.Status == System.Net.NetworkInformation.IPStatus.Success)
            {
                label3.Text = HostPingReply3.RoundtripTime.ToString() + " ms";
                label33.Text = "ip: " + HostPingReply3.Address.ToString();
                label3333.Text = "TTL: " + HostPingReply3.Options.Ttl.ToString();

                OnOff[2] = true;
            }
            else
            {
                label3.Text = "-";
                label33.Text = "-";
                label3333.Text = "-";

                OnOff[2] = false;
            }
        }

        private void HostPingReplyL4(System.Net.NetworkInformation.PingReply HostPingReply4)
        {
            if (HostPingReply4.Status == System.Net.NetworkInformation.IPStatus.Success)
            {
                label4.Text = HostPingReply4.RoundtripTime.ToString() + " ms";
                label44.Text = "ip: " + HostPingReply4.Address.ToString();
                label4444.Text = "TTL: " + HostPingReply4.Options.Ttl.ToString();
            }
            else
            {
                label4.Text = "-";
                label44.Text = "-";
                label4444.Text = "-";
            }
        }

        private void HostPingReplyL5(System.Net.NetworkInformation.PingReply HostPingReply5)
        {
            if (HostPingReply5.Status == System.Net.NetworkInformation.IPStatus.Success)
            {
                label5.Text = HostPingReply5.RoundtripTime.ToString() + " ms";
                label55.Text = "ip: " + HostPingReply5.Address.ToString();
                label5555.Text = "TTL: " + HostPingReply5.Options.Ttl.ToString();
            }
            else
            {
                label5.Text = "-";
                label55.Text = "-";
                label5555.Text = "-";
            }
        }

        private void StopThread()
        {
            listBox1.Items.Add(DateTime.Now.ToString() + " - Закрытие программы");
            LogFile.WriteLine(DateTime.Now.ToString() + " - Закрытие программы");
            LogFile.Close();

            thread1.Abort();//прерываем поток
            thread1.Join(50);//таймаут на завершение
            thread1 = null;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (OnOff[0] == false && OnOff[1] == false && OnOff[2] == false)
            {
                if (OnFlag == true)
                {
                    listBox1.Items.Add(DateTime.Now.ToString() + " - Интернет отключен");
                    OnFlag = false;

                    LogFile.WriteLine(DateTime.Now.ToString() + " - Интернет отключен");
                }
            }
            else
            {
                if (OnFlag == false)
                {
                    listBox1.Items.Add(DateTime.Now.ToString() + " - Интернет включен");
                    OnFlag = true;

                    LogFile.WriteLine(DateTime.Now.ToString() + " - Интернет включен");
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AppStart == true)
            StopThread();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            ThreadPingCl();

            timer1.Enabled = true;
            timer2.Enabled = false;
        }
    }

    public partial class PingCl
    {
        public void SendPing(object param, string[] host)
        {
            while (true)
            {
                SynchronizationContext context = (SynchronizationContext)param;

                System.Net.NetworkInformation.PingOptions options = new System.Net.NetworkInformation.PingOptions();

                // Use the default Ttl value which is 128,
                // but change the fragmentation behavior.
                options.DontFragment = true;

                // Create a buffer of 32 bytes of data to be transmitted.
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 4000;

                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply pingReply = ping.Send(host[0], timeout, buffer, options);
                System.Net.NetworkInformation.PingReply pingReply2 = ping.Send(host[1], timeout, buffer, options);
                System.Net.NetworkInformation.PingReply pingReply3 = ping.Send(host[2], timeout, buffer, options);
                System.Net.NetworkInformation.PingReply pingReply4 = ping.Send(host[3], timeout, buffer, options);
                System.Net.NetworkInformation.PingReply pingReply5 = ping.Send(host[4], timeout, buffer, options);

                //try
                //
                //if (context != null)
                context.Send(OnHostPingReply, pingReply);
                context.Send(OnHostPingReply2, pingReply2);
                context.Send(OnHostPingReply3, pingReply3);
                context.Send(OnHostPingReply4, pingReply4);
                context.Send(OnHostPingReply5, pingReply5);
                //}
                //catch { }
                //finally { }

                Thread.Sleep(2000);

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public void OnHostPingReply(object i)
        {
            if (HostPingReply != null)
            HostPingReply((System.Net.NetworkInformation.PingReply) i);
        }

        public event Action<System.Net.NetworkInformation.PingReply> HostPingReply;

        public void OnHostPingReply2(object i)
        {
            if (HostPingReply2 != null)
                HostPingReply2((System.Net.NetworkInformation.PingReply)i);
        }

        public event Action<System.Net.NetworkInformation.PingReply> HostPingReply2;

        public void OnHostPingReply3(object i)
        {
            if (HostPingReply3 != null)
                HostPingReply3((System.Net.NetworkInformation.PingReply)i);
        }

        public event Action<System.Net.NetworkInformation.PingReply> HostPingReply3;

        public void OnHostPingReply4(object i)
        {
            if (HostPingReply4 != null)
                HostPingReply4((System.Net.NetworkInformation.PingReply)i);
        }

        public event Action<System.Net.NetworkInformation.PingReply> HostPingReply4;

        public void OnHostPingReply5(object i)
        {
            if (HostPingReply5 != null)
                HostPingReply5((System.Net.NetworkInformation.PingReply)i);
        }

        public event Action<System.Net.NetworkInformation.PingReply> HostPingReply5;
    }
}


//label111.text = host[0] = h1_adr;
//label222.text = host[1] = h2_adr;
//label333.text = host[2] = h3_adr;
//label444.text = host[3] = h4_adr;
//label555.text = host[4] = h5_adr;